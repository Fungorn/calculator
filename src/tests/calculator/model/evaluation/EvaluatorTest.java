package tests.calculator.model.evaluation;

import calculator.model.evaluation.Evaluator;
import org.junit.Test;
import org.mockito.Mock;

import static junit.framework.TestCase.assertEquals;

public class EvaluatorTest {

    @Mock
    Evaluator evaluator;

    @Test
    public void whenCorrectPositiveInput_thenSuccess() {
        final String expressionText = "4 - 5";
        evaluator = new Evaluator(expressionText);
        final int result = evaluator.evaluate();
        assertEquals(-1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenIncorrectInput_thenException() {
        final String expressionText = "2)(4";
        evaluator = new Evaluator(expressionText);
        evaluator.evaluate();
    }

    @Test
    public void whenCorrectNegativeInput_thenSuccess() {
        final String expressionText = "-(2×4+1)";
        evaluator = new Evaluator(expressionText);
        final int result = evaluator.evaluate();
        assertEquals(-9, result);
    }

    @Test
    public void whenZeroInput_thenSuccess() {
        final String expressionText = "5-04";
        evaluator = new Evaluator(expressionText);
        final int result = evaluator.evaluate();
        assertEquals(1, result);
    }
}
