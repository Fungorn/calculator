package tests.calculator.model.evaluation;

import calculator.model.evaluation.ShuntingYard;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class ShuntingYardTest {

    @Test
    public void whenCorrectInfix_thenTrue() {
        final String result = ShuntingYard.postfix("-(24-11)×2-1");
        assertEquals("24 11 - 2 × - 1 - ", result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenIncorrectInfix_thenException1() {
        ShuntingYard.postfix("2)(4");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenIncorrectInfix_thenException2() {
        ShuntingYard.postfix("((2-4)");
    }
}
