package tests.calculator.model.operators;

import calculator.model.operators.Expression;
import calculator.model.operators.ExpressionFactory;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class ExpressionFactoryTest {

    @Test(expected = IllegalArgumentException.class)
    public void whenIncorrectInput_thenException() {
        ExpressionFactory.createExpression("");
    }

    @Test
    public void whenNumberInput_thenNumberExpression() {
        Expression expression = ExpressionFactory.createExpression("1");
        assertEquals(1, expression.interpret());
    }

    @Test
    public void whenExpressionInput_thenSuccess() {
        Expression expression = ExpressionFactory.createExpression("2 1 -");
        assertEquals(1, expression.interpret());
    }
}