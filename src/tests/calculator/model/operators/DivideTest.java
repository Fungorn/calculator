package tests.calculator.model.operators;

import calculator.model.operators.Divide;
import calculator.model.operators.Expression;
import calculator.model.operators.Number;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;

public class DivideTest {
    @Mock
    Expression divide;

    @Test(expected = NullPointerException.class)
    public void whenInitByNull_thenException() {
        divide = new Divide(null, null);
    }

    @Test(expected = ArithmeticException.class)
    public void whenLeftIsZero_thenException() {
        divide = new Divide(Number.valueOf(0), Number.valueOf(1));
        divide.interpret();
    }
}