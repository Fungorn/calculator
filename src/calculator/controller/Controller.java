package calculator.controller;

import calculator.model.evaluation.Evaluator;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.util.Arrays;
import java.util.regex.Pattern;

public class Controller {
    private String lastChar;
    private Evaluator evaluator;

    @FXML
    private Label enteredText;

    @FXML
    private Button
            zeroButton, oneButton, twoButton, threeButton, fourButton,
            fiveButton, sixButton, sevenButton, eightButton, nineButton,
            leftBracketButton, rightBracketButton,
            divButton, mulButton, subButton, addButton;

    @FXML
    public void initialize() {
        Button[] numbers = {oneButton, twoButton, threeButton,
                fourButton, fiveButton, sixButton,
                sevenButton, eightButton, nineButton,
                zeroButton};

        Button[] operators = {divButton, mulButton, subButton, addButton};

        lastChar = "";
        enteredText.setText(lastChar);

        Arrays.asList(numbers).forEach(button -> button.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            final String text = enteredText.getText();
            if (!lastChar.equals(")")) {
                enteredText.setText(text + button.getText());
                lastChar = button.getText();
            }
        }));

        Arrays.asList(operators).forEach(button -> button.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            final String text = enteredText.getText();
            if (!Pattern.matches("[-+×÷]", lastChar)) {
                enteredText.setText(text + button.getText());
                lastChar = button.getText();
            }
        }));

        leftBracketButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            final String text = enteredText.getText();
            if (text.isEmpty() || !lastChar.equals(")") && !Pattern.matches("\\d", lastChar)) {
                enteredText.setText(text + leftBracketButton.getText());
                lastChar = leftBracketButton.getText();
            }
        });

        rightBracketButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            final String text = enteredText.getText();
            if (!lastChar.equals("(") && !Pattern.matches("[-+×÷]", lastChar) && !text.isEmpty()) {
                enteredText.setText(text + rightBracketButton.getText());
                lastChar = rightBracketButton.getText();
            }
        });
    }

    @FXML
    public void onEqvButtonClick() {
        try {
            evaluator = new Evaluator(enteredText.getText());
            int result = evaluator.evaluate();
            enteredText.setText(String.valueOf(result));
        } catch (Exception exception) {
            enteredText.setText("");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(exception.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    public void onClearButtonClick() {
        enteredText.setText("");
    }

}
