package calculator.model.evaluation;

import calculator.model.operators.Expression;
import calculator.model.operators.ExpressionFactory;

public final class Evaluator {

    private final Expression syntaxTree;

    public Evaluator(final String expressionText) {
        String postfix = ShuntingYard.postfix(expressionText);
        this.syntaxTree = buildSyntaxTree(postfix);
    }

    public int evaluate() {
        return this.syntaxTree.interpret();
    }

    private static Expression buildSyntaxTree(final String prefix) {
        return ExpressionFactory.createExpression(prefix);
    }

}