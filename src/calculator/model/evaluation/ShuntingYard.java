package calculator.model.evaluation;

import java.util.Stack;
import java.util.StringTokenizer;

public final class ShuntingYard {
    private static boolean isInfix(String str) {
        Stack<Character> stack = new Stack<>();

        char c;

        for (int i = 0; i < str.length(); i++) {
            c = str.charAt(i);

            if (c == '(')
                stack.push(c);
            else if (c == ')')
                if (stack.empty())
                    return false;
                else if (stack.peek() == '(')
                    stack.pop();
                else
                    return false;
        }
        return stack.empty();
    }

    public static String postfix(String infix) {
        /* To find out the precedence, we take the index of the
           token in the ops string and divide by 2 (in stack.peek() / 2).
           This will give us: 0, 0, 1, 1
           */
        if (!isInfix(infix))
            throw new IllegalArgumentException("Typed expression isn't in correct infix format.");

        final String ops = "-+÷×";
        final String delimiters = "()" + ops;

        StringTokenizer stringTokenizer = new StringTokenizer(infix, delimiters, true);
        StringBuilder stringBuilder = new StringBuilder();
        Stack<Integer> stack = new Stack<>();

        char c;
        int idx;

        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken().trim();
            if (token.isEmpty())
                continue;
            c = token.charAt(0);
            idx = ops.indexOf(c);

            // check for operator
            if (idx != -1) {
                if (stack.isEmpty())
                    stack.push(idx);
                else {
                    while (!stack.isEmpty()) {
                        int precedence2 = stack.peek() / 2;
                        int precedence1 = idx / 2;
                        if (precedence2 > precedence1 || (precedence2 == precedence1))
                            stringBuilder.append(ops.charAt(stack.pop())).append(' ');
                        else break;
                    }
                    stack.push(idx);
                }
            } else if (c == '(') {
                // -2 is precedence for '('
                stack.push(-2);
            } else if (c == ')') {
                while (stack.peek() != -2)
                    stringBuilder.append(ops.charAt(stack.pop())).append(' ');
                stack.pop();
            } else {
                stringBuilder.append(token).append(' ');
            }
        }
        while (!stack.isEmpty())
            stringBuilder.append(ops.charAt(stack.pop())).append(' ');
        return stringBuilder.toString();
    }

}
