package calculator.model.operators;

public interface Expression {
    int interpret();
}

