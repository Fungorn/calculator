package calculator.model.operators;

public final class Divide implements Expression {

    private final Expression leftOperand;
    private final Expression rightOperand;

    public Divide(final Expression left,
           final Expression right) {
        if (left == null || right == null) throw new NullPointerException();
        this.leftOperand = left;
        this.rightOperand = right;
    }

    @Override
    public int interpret() {
        final int numerator = this.rightOperand.interpret();
        final int denominator = this.leftOperand.interpret();
        if (denominator == 0) {
            throw new ArithmeticException("Divide by zero!");
        }
        return numerator / denominator;
    }

}