package calculator.model.operators;

import java.util.EmptyStackException;
import java.util.Stack;

public class ExpressionFactory {

    private static final String EXPRESSION_DELIMITER = " ";
    private static final String PLUS_TOKEN = "+";
    private static final String MINUS_TOKEN = "-";
    private static final String MULTIPLY_TOKEN = "×";
    private static final String DIVIDE_TOKEN = "÷";

    private ExpressionFactory() {
    }

    public static Expression createExpression(final String prefix) {
        final Stack<Expression> expressionStack = new Stack<>();
        for (final String token : prefix.split(EXPRESSION_DELIMITER)) {
            expressionStack.push(parseToken(token, expressionStack));
        }
        return expressionStack.pop();
    }

    private static Expression parseToken(final String token,
                                         final Stack<Expression> expressionStack) {
        Expression parsedExpression = null;
        Expression leftOperand = null;
        Expression rightOperand = null;

        switch (token) {
            case PLUS_TOKEN:
                try {
                    leftOperand = expressionStack.pop();
                    rightOperand = expressionStack.pop();
                    parsedExpression = new Plus(leftOperand, rightOperand);
                } catch (EmptyStackException exception) {
                    exception.printStackTrace();
                }
                break;
            case MINUS_TOKEN:
                try {
                    leftOperand = expressionStack.pop();
                    rightOperand = expressionStack.pop();

                    parsedExpression = new Minus(leftOperand, rightOperand);
                } catch (EmptyStackException exception) {
                    if (leftOperand != null)
                        parsedExpression = new Minus(leftOperand, Number.valueOf(0));
                }
                break;
            case MULTIPLY_TOKEN:
                try {
                    leftOperand = expressionStack.pop();
                    rightOperand = expressionStack.pop();
                    parsedExpression = new Multiply(leftOperand, rightOperand);
                } catch (EmptyStackException exception) {
                    exception.printStackTrace();
                }
                break;
            case DIVIDE_TOKEN:
                try {
                    leftOperand = expressionStack.pop();
                    rightOperand = expressionStack.pop();
                    parsedExpression = new Divide(leftOperand, rightOperand);
                } catch (EmptyStackException exception) {
                    exception.printStackTrace();
                }
                break;
            default:
                parsedExpression = parseNumberToken(token);
                break;
        }
        return parsedExpression;
    }

    private static Expression parseNumberToken(final String token) {
        if (token.matches("[0-9]+")) {
            return Number.valueOf(Integer.parseInt(token));
        }
        throw new IllegalArgumentException("Invalid symbol: " + token);
    }

}
