package calculator.model.operators;

final class Multiply implements Expression {

    private final Expression leftOperand;
    private final Expression rightOperand;

    Multiply(final Expression left,
             final Expression right) {
        if (left == null || right == null) throw new NullPointerException();
        this.leftOperand = left;
        this.rightOperand = right;
    }

    @Override
    public int interpret()  {
        return this.leftOperand.interpret() * this.rightOperand.interpret();
    }

}